#include <string>
#include <iostream>

using namespace std;


using std::string;

string randDNA(int seed, string bases, int n)
{

	string letters;
	
	std:: mt19937 eng1(seed);
	
		int min = 1;
		int max = bases.size();
		
	std:: uniform_int_distribution<>unifrm(min,max);
	
	for (int b = 0; b < n; b++)
	{
		letters += bases [unifrm(eng1)-1];
	}
	
	return letters;
}
